# *💖Welcome to my profile💖* 
<br>

## About me ⭐
<details>
    <summary>click here✨</summary>
    <ul>
            <li> I am 19y </li>
            <li> I am living in Brazil </li>
            <li> I like cartoon, games, music and drawing </li>
            <li> If you like this profile, starred and follow me 💕</li>
     </ul>
</details>       
<br>

## My Activity  
<br>
    <div>
     <details>
     <summary>🏆My Trophies🏆</summary>
    <br />
<p align="center">
        <img src="https://github-profile-trophy.vercel.app/?username=MilenaMartini&theme=darkhub&margin-w=15" alt="Trophies GitHub" />
</p>
  
 </details> 
    </div>
    <br />
 
<div align='center'>
  <img height="150em" src="http://github-readme-streak-stats.herokuapp.com?user=MilenaMartini&theme=github_dark&hdate_format=j%20M%5B%20Y%5D&stroke=DBDADA&background=0D1117&border=fff&ring=f22727&fire=b30d1e&currStreakNum=FFFF&sideNums=FFFF&currStreakLabel=f22727&sideLabels=f22727&dates=FFFF"/><br>
    
  <img width="400em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=MilenaMartini&layout=compact&langs_count=10&theme=github_dark&title_color=f22727" />      
        
<div style="display: inline_block" align="center"><br>

 </div>
  </div>
 
 ## Programming language and frameworks
 <br> 
  <p align="center">
    <img src="https://skillicons.dev/icons?i=cs,css,html,js,php,react,sass" />
  </p>
  <br />
 
 ## Programs
 
   <br> 
  <p align="center">
    <img src="https://skillicons.dev/icons?i=mysql,visualstudio,vscode" />
  </p>
  <br />

## 
    
![Snake animation](https://github.com/MilenaMartini/MilenaMartini/blob/output/github-contribution-grid-snake.svg)

</div>

## Where find me

<details>
    <summary>click here✨</summary>
    
<p align="center">
  <a href="https://linkedin.com/in/MilenaMartini" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="MilenaMartini" height="35" width="40" /></a> 
  <a href="https://instagram.com/lena_miart" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="MilenaMartini" height="35" width="40" /></a>


</p>
</details>

<p align="end">
<a href="https://visitorbadge.io/status?path=https%3A%2F%2Fgithub.com%2FMilenaMartini%2FMilenaMartini%2Fedit%2Fmain%2FREADME.md"><img src="https://api.visitorbadge.io/api/visitors?path=https%3A%2F%2Fgithub.com%2FMilenaMartini%2FMilenaMartini%2Fedit%2Fmain%2FREADME.md&label=Visitantes&labelColor=%231c1c1c&countColor=%23ff0000" /></a>
</p>
